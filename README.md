# Vending Machine
This code simulate a vending machine with basics commands; written in PHP 8 and uses [Symfony 5.3](https://symfony.com/what-is-symfony). It's a short example of the adaptation of [ADR pattern](https://en.wikipedia.org/wiki/Action%E2%80%93domain%E2%80%93responder) with a single command app and the use of services. This example doesn't use a database, store all necessary data in json files.

- [Requirements](#Requirements)
- [Installation](#Installation)
- [Running](#Running)

### Requirements
* Operating system Windows , Linux, Mac
* Composer
* Git
* Docker
* Docker-compose



### Installation
* Clone the repository
```bash
$ git clone https://Jcxnet@bitbucket.org/Jcxnet/vending.machine.git
```

* Install dependencies
```bash
$ composer install
```

* Create the docker machines
```bash
$ docker-compose up -d
```

### Running
* Once upon the docker machines are working, open a terminal in the PHP instance and run the machine app
```bash
$/srv/www# ./public/machine 

Vending Machine is ready! > 
```
### Command list
  
| Command | Description                    |
| ------------- | ------------------------------ |
| `1, 0.10, 0.25`| Inserts coins in the machine; only accepts these ones: _0.05, 0.10, 0.25 and 1_.       |
| `RETURN-COIN`   | Returns all inserted money     |
| `GET-WATER`   | Select a Water item from the machine, the price is 0.65     |
| `GET-JUICE`   | Select a Juice item from the machine, the price is 1.00     |
| `GET-SODA`   | Select a Soda item from the machine, the price is 1.50     |
| `SERVICE`   | Open/closes the machine service mode to update items and coins     |
| `EXIT`   | Closes the machine app     |

> **Note:** It's possible combine inserted coins with an item
```bash
   Vending Machine is ready! > 1, GET-JUICE
   +-------+ Credit +-------+
   | Value | Amount | Total |
   +-------+--------+-------+
   | 1.00  | 1      | 1.00  |
   +-------+--------+-------+
   |            Total: 1.00 |
   +-------+--------+-------+                                                                                                                      
   [OK] -> JUICE  
```
```bash
Vending Machine is ready! > 1, 0.25, 0.25, GET-SODA
+-------+ Credit +-------+
| Value | Amount | Total |
+-------+--------+-------+
| 0.25  | 2      | 0.50  |
| 1.00  | 1      | 1.00  |
+-------+--------+-------+
|            Total: 1.50 |
+-------+--------+-------+

 [OK] -> SODA  
```
> **Note:**  If you insert more coins than item's price, the machine returns the exact change

```bash
Vending Machine is ready! > 1, GET-WATER
+-------+ Credit +-------+
| Value | Amount | Total |
+-------+--------+-------+
| 1.00  | 1      | 1.00  |
+-------+--------+-------+
|            Total: 1.00 |
+-------+--------+-------+

[OK] -> WATER, 0.25, 0.10
```


### Service command list
* To change to Service mode use the SERVICE mode
```bash
Vending Machine is ready! > SERVICE

[WARNING] Vending Machine open (service mode ON)  

Vending Machine in service mode (!) >  
```

| Command | Description                    |
| ------------- | ------------------------------ |
| `1, 0.10, 0.25`| Inserts coins directly to the machine; only accepts these ones: _0.05, 0.10, 0.25 and 1_.       |
| `RETURN-COIN`   | Returns all the machine money    |
| `1, SET-WATER`   | Insert a Water item into the machine|
| `1, SET-JUICE`   | Insert a Juice item into the machine|
| `1, SET-SODA`   | Insert a Soda item into the machine|
| `SERVICE`   | Closes the machine service mode 
| `EXIT`   | Closes the machine app     |

> **Note:**  When inserts an item in the machine, you can define a value between 1 an 99

```bash
Vending Machine in service mode (!) > 5, SET-WATER
+-------- Products +-------+
| Product | Amount | Price |
+---------+--------+-------+
| WATER   | 5      | 0.65  |
| JUICE   | 0      | 1.00  |
| SODA    | 0      | 1.50  |
+---------+--------+-------+
```


