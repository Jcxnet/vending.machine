<?php

declare(strict_types=1);


namespace App\Domain\Traits;

use App\Domain\Entity\Machine;
use App\Domain\Exception\Console\EmptyException;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Helper\TableCell;
use Symfony\Component\Console\Helper\TableCellStyle;
use Symfony\Component\Console\Helper\TableSeparator;
use Symfony\Component\Console\Output\ConsoleOutput;
use Exception;
use Symfony\Component\Console\Output\OutputInterface;

trait ConsoleOutputTrait
{
    public function renderException(ConsoleOutput $output, Exception $e): void
    {
        $table = new Table($output);
        $table
            ->setStyle('default')
            ->setHeaderTitle('Error')
            ->setHeaders(['Code', 'Message'])
            ->setRows([
                [
                    $e->getCode(),
                    $e->getMessage(),
                ],
            ])
        ;
        $table->render();

        throw new EmptyException();
    }

    public function renderMachineInfo(ConsoleOutput $output, Machine $machine): void
    {
        $table = new Table($output);
        $table
            ->setStyle('box')
            ->setHeaderTitle('Machine')
            ->setHeaders(['Version', $machine->getVersion()]);
        $table->render();

        $this->renderProducts('Machine Products', $machine->getProducts(), $output);
        $this->renderCoins('Machine Coins', $machine->getCoins(), $output);
        $this->renderCoins('Machine Credit', $machine->getCredit(), $output);

    }

    public function renderProducts(string $title, array $products, OutputInterface $output): void
    {
        $table = new Table($output);
        $table
            ->setHeaderTitle($title)
            ->setHeaders(['Product', 'Amount', 'Price']);
        $rows = [];
        foreach ($products as $product) {
            $rows[] =[$product->getName(), $product->getAmount(), sprintf("%.2f",$product->getPrice())];
        }
        $table->setRows($rows);
        $table->render();
    }

    public function renderCoins(string $title, array $coins, OutputInterface $output): void
    {
        $table = new Table($output);
        $table
            ->setHeaderTitle($title)
            ->setHeaders(['Value', 'Amount', 'Total']);
        $rows = [];
        $total = 0.0;
        foreach ($coins as $coin) {
           if (0 < $coin->getAmount()) {
               $rows[] = [
                   sprintf("%.2f",$coin->getValue()),
                   $coin->getAmount(),
                   sprintf("%.2f",$coin->getValue() * $coin->getAmount())
               ];
               $total += $coin->getValue() * $coin->getAmount();
           }
        }
        $rows[] = new TableSeparator();
        $rows[] = [new TableCell(sprintf("Total: %.2f",$total),['colspan' => 3, 'style' => new TableCellStyle(['align' => 'right'])])];
        $table->setRows($rows);
        $table->render();
    }
}
