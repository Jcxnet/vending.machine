<?php

declare(strict_types=1);

namespace App\Domain\Exception\Service;

use App\Domain\Exception\DomainError;

final class ServiceNotFound extends DomainError
{

    public function errorCode(): int
    {
        return 404;
    }

    protected function errorMessage(): string
    {
        return 'machine.service.not.found';
    }
}
