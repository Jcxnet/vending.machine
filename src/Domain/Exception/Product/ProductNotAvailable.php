<?php

declare(strict_types=1);

namespace App\Domain\Exception\Product;

use App\Domain\Exception\DomainError;

final class ProductNotAvailable extends DomainError
{

    public function errorCode(): int
    {
        return 404;
    }

    protected function errorMessage(): string
    {
        return 'product.not.available';
    }
}
