<?php

declare(strict_types=1);

namespace App\Domain\Exception\Action;

use App\Domain\Exception\DomainError;

final class NoAction extends DomainError
{

    public function errorCode(): int
    {
        return 400;
    }

    protected function errorMessage(): string
    {
        return 'machine.no.action.selected';
    }
}
