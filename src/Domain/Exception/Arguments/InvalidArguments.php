<?php

declare(strict_types=1);

namespace App\Domain\Exception\Arguments;

use App\Domain\Exception\DomainError;

final class InvalidArguments extends DomainError
{

    public function errorCode(): int
    {
        return 400;
    }

    protected function errorMessage(): string
    {
        return 'machine.action.invalid.arguments';
    }
}
