<?php

declare(strict_types=1);

namespace App\Domain\Exception\Credit;

use App\Domain\Exception\DomainError;

final class CreditNotEnough extends DomainError
{

    public function errorCode(): int
    {
        return 404;
    }

    protected function errorMessage(): string
    {
        return 'credit.not.enough';
    }
}
