<?php

declare(strict_types=1);

namespace App\Domain\Exception\Machine;

use App\Domain\Exception\DomainError;

final class ServiceModeInactive extends DomainError
{

    public function errorCode(): int
    {
        return 400;
    }

    protected function errorMessage(): string
    {
        return 'machine.service.mode.inactive';
    }
}
