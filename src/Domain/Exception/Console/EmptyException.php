<?php

declare(strict_types=1);

namespace App\Domain\Exception\Console;

use App\Domain\Exception\DomainError;

final class EmptyException extends DomainError
{

    public function errorCode(): int
    {
        return 400;
    }

    protected function errorMessage(): string
    {
        return '';
    }
}
