<?php

declare(strict_types=1);

namespace App\Domain\Exception\File;

use App\Domain\Exception\DomainError;

final class FileNotFound extends DomainError
{

    public function errorCode(): int
    {
        return 404;
    }

    protected function errorMessage(): string
    {
        return 'machine.file.not.found';
    }
}
