<?php

declare(strict_types=1);

namespace App\Domain\Exception\File;

use App\Domain\Exception\DomainError;

final class FileSaveFails extends DomainError
{

    public function errorCode(): int
    {
        return 400;
    }

    protected function errorMessage(): string
    {
        return 'machine.file.save.fails';
    }
}
