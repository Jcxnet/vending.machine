<?php

declare(strict_types=1);


namespace App\Domain\Command\Machine;

use App\Domain\ActionService;
use App\Domain\Entity\Action;
use App\Domain\Exception\Action\NoAction;
use App\Domain\Exception\Service\ServiceNotFound;
use App\Domain\Traits\ConsoleOutputTrait;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class MachineOperateCommand extends Command
{
    use ConsoleOutputTrait;

    protected static $defaultName ='machine:operate';

    private ?ContainerBuilder $container;

    public function __construct()
    {
        $this->container = null;
        parent::__construct();
    }

    public function setContainer(ContainerBuilder $container): self
    {
        $this->container = $container;

        return $this;
    }

    protected function configure()
    {
        $this->setHelp('This command operates the vending machine');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $action = new Action('','');
        $helper = $this->getHelper('question');
        $question = new Question($this->questionMode($output));

        $question->setTrimmable(true);

        $question->setValidator(function ($answer) use ($input, $output, &$action) {
            if ("EXIT" !== $answer && "exit" !== $answer) {
                $arguments = $this->formatArguments($answer);
                $action = $this->getAction($arguments);

                if (!$action->isAction()) {
                   $this->renderException($output, new NoAction());
                } else {
                    if ($action->hasService()) {
                        $action->setContainer($this->container);
                        $this->runAction($action, $input, $output);
                    }
                }
                $this->execute($input, $output);
            }

            return $answer;
        });

        $question->setMaxAttempts(null);
        $helper->ask($input, $output, $question);

        return $action->getCode();
    }

    private function runAction(Action $action, InputInterface $input, OutputInterface $output): void
    {
        if (!$this->container->hasDefinition($action->getService())) {
            $this->renderException($output, new ServiceNotFound());
        }
        /** @var ActionService $service */
        $service = $this->container->get($action->getService());
        $service($action, $input, $output);
    }

    private function getAction(array $arguments): Action
    {
        if (0 == count($arguments)) {
            return new Action('', '');
        }
        $consoleAction = $this->container->get('machine.console.action');

        return $consoleAction($arguments);
    }

    private function formatArguments(?string $arguments): array
    {
        if (null == $arguments) {
            return [];
        }
        $arguments = explode(',', $arguments);
        $list = [];
        foreach ($arguments as $argument) {
            if (0 < substr_count($argument,',')) {
                $items = explode(',', $argument);
                foreach ($items as $item) {
                    $list[] = $item;
                }
            } else {
                $list[] = $argument;
            }
        }
        $values = [];
        foreach ($list as $item) {
            if ('' !==$item) {
                $values[] = trim(str_replace(',','',$item));
            }
        }

        return $values;
    }

    private function questionMode(OutputInterface $output): string
    {
        $machineInfo = $this->container->get('machine.info');
        $machineInfo($this->container, $output);
        if ($machineInfo->isServiceMode()){
            return "<fg=yellow>Vending Machine in service mode (!) ></> ";
        }

        return "<fg=green>Vending Machine is ready! ></> ";
    }
}
