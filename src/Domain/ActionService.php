<?php

declare(strict_types=1);

namespace App\Domain;

use App\Domain\Entity\Action;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

interface ActionService
{
    public function __invoke(Action $action, InputInterface $input, OutputInterface $output);
}
