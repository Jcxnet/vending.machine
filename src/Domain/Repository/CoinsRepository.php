<?php

declare(strict_types=1);

namespace App\Domain\Repository;

use App\Domain\Entity\Coin;

interface CoinsRepository
{
    public function get(string $value): ?Coin;
    public function getAll(): array;
    public function save(Coin $coin): void;
    public function saveAll(array $coins): void;
}
