<?php

declare(strict_types=1);

namespace App\Domain\Repository;

use App\Domain\Entity\Machine;

interface MachineRepository
{
    public function get(): Machine;
    public function save(Machine $machine): void;
}
