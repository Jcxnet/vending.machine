<?php

declare(strict_types=1);

namespace App\Domain\Repository;

use App\Domain\Entity\Product;

interface ProductsRepository
{
    public function get(string $name): ?Product;
    public function getAll(): array;
    public function save(Product $product): void;
}
