<?php

declare(strict_types=1);

namespace App\Domain\Repository;

interface CreditRepository
{
    public function getAll(): array;
    public function saveAll(array $coins): void;
}
