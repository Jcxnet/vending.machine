<?php

declare(strict_types=1);

namespace App\Domain\Validator;

interface Validator
{
    public function validate(array $data): bool;
}
