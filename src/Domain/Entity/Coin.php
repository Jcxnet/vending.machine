<?php

declare(strict_types=1);

namespace App\Domain\Entity;

use App\Domain\Entity;

class Coin implements Entity
{
    public function __construct(
        private float $value,
        private int $amount
    ){}

    public function getValue(): float
    {
        return $this->value;
    }

    public function setValue(float $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getAmount(): int
    {
        return $this->amount;
    }

    public function setAmount(int $amount): self
    {
        $this->amount = $amount;

        return $this;
    }


}
