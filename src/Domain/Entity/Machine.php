<?php

declare(strict_types=1);

namespace App\Domain\Entity;

use App\Domain\Entity;

class Machine implements Entity
{
    private string $version;
    private bool $serviceMode;
    private ?array $products;
    private ?array $coins;
    private ?array $credit;

    public function __construct()
    {
        $this->products = [];
        $this->coins = [];
        $this->credit = [];
        $this->version = "0.0.0";
        $this->serviceMode = false;
    }

    public function getProducts(): ?array
    {
        return $this->products;
    }

    public function setProducts(?array $products): self
    {
        $this->products = $products;

        return $this;
    }

    public function getCoins(): ?array
    {
        return $this->coins;
    }

    public function setCoins(?array $coins): self
    {
        $this->coins = $coins;

        return $this;
    }

    public function getCredit(): ?array
    {
        return $this->credit;
    }

    public function setCredit(?array $credit): self
    {
        $this->credit = $credit;

        return $this;
    }

    public function getVersion(): string
    {
        return $this->version;
    }

    public function setVersion(string $version): self
    {
        $this->version = $version;

        return $this;
    }

    public function setServiceMode(bool $serviceMode): self
    {
        $this->serviceMode = $serviceMode;

        return $this;
    }

    public function isServiceMode(): bool
    {
        return $this->serviceMode;
    }



}
