<?php

declare(strict_types=1);

namespace App\Domain\Entity;

use App\Domain\Entity;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class Action implements Entity
{
    const ACTION_NOT_FOUND = 0;
    const ACTION_INSERT_COINS = 1;
    const ACTION_RETURN_COINS = 2;
    const ACTION_SELL_PRODUCT = 3;
    const ACTION_SERVICE = 4;
    const ACTION_SET_PRODUCT = 5;
    const ACTION_EXIT = 6;

    private ?string $service;
    private array $arguments;
    private ?ContainerBuilder $container;

    public function __construct(
        private string $action,
        private string $description,
        private int $code = self::ACTION_NOT_FOUND
    ){
        $this->service = null;
        $this->arguments = [];
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function setAction(string $action): self
    {
        $this->action = $action;

        return $this;
    }

    public function getAction(): string
    {
        return $this->action;
    }

    public function getCode(): int
    {
        return $this->code;
    }

    public function setCode(int $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getService(): ?string
    {
        return $this->service;
    }

    public function setService(?string $service): self
    {
        $this->service = $service;

        return $this;
    }

    public function getArguments(): array
    {
        return $this->arguments;
    }

    public function setArguments(array $arguments): self
    {
        $this->arguments = $arguments;

        return $this;
    }

    public function isAction(): bool
    {
        return (self::ACTION_NOT_FOUND !== $this->code);
    }

    public function hasService(): bool
    {
        return (null !== $this->service);
    }

    public function setContainer(?ContainerBuilder $container): self
    {
        $this->container = $container;

        return $this;
    }

    public function getContainer(): ?ContainerBuilder
    {
        return $this->container;
    }



}
