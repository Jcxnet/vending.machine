<?php

declare(strict_types=1);

namespace App\Domain\Service;

use App\Domain\Entity\Machine;
use App\Domain\Exception\Service\ServiceNotFound;
use App\Domain\Repository\MachineRepository;
use App\Domain\Traits\ConsoleOutputTrait;
use App\Infrastructure\Repository\DataCoinsRepository;
use App\Infrastructure\Repository\DataCreditRepository;
use App\Infrastructure\Repository\DataMachineRepository;
use App\Infrastructure\Repository\DataProductsRepository;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class MachineService
{
    use ConsoleOutputTrait;

    private MachineRepository $repository;
    private FileDataManager $dataManager;

    public function __invoke(
        ContainerBuilder $container,
        OutputInterface $output
    ){
        if (!$dataManager = $container->get('machine.datamanager')) {
            $this->renderException($output, new ServiceNotFound());
        }
        $this->dataManager = $dataManager;
        $this->repository = new DataMachineRepository($dataManager);
    }

    public function getMachine(): Machine
    {
        $machine = $this->repository->get();
        $coinsRepository = new DataCoinsRepository($this->dataManager);
        $creditRepository = new DataCreditRepository($this->dataManager);
        $productsRepository = new DataProductsRepository($this->dataManager);

        $machine->setCoins($coinsRepository->getAll());
        $machine->setCredit($creditRepository->getAll());
        $machine->setProducts($productsRepository->getAll());

        return $machine;
    }

    public function isServiceMode(): bool
    {
        $machine = $this->repository->get();

        return $machine->isServiceMode();
    }

    public function setServiceMode(bool $serviceMode): void
    {
        $machine = $this->repository->get();
        $machine->setServiceMode($serviceMode);
        $this->repository->save($machine);
    }
}
