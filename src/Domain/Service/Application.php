<?php

declare(strict_types=1);

namespace App\Domain\Service;

use App\Domain\Entity\Action;
use Exception;
use Symfony\Component\Console\Application as BaseApplication;
use App\Domain\Command\Machine\MachineOperateCommand;
use App\Domain\Traits\ConsoleOutputTrait;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class Application extends BaseApplication
{
    use ConsoleOutputTrait;

    private static string $name = "Vending Machine";
    private static string $version = "1.0.0";
    private ContainerBuilder $container;

    public function __construct(){
        $this->setName(static::$name);
        $this->setVersion(static::$version);
        parent::__construct($this->getName(), $this->getVersion());
    }

    public function setContainer(ContainerBuilder $container): self
    {
        $this->container = $container;

        return $this;
    }

    public function execute(): int
    {
        $command = new MachineOperateCommand();
        $command->setContainer($this->container);
        $this->add($command);
        $this->setDefaultCommand($command->getName(), true);

        $this->setCatchExceptions(false);

        try{
            $output = new ConsoleOutput();
            return $this->run(null, $output);
        } catch (Exception $e) {
            $this->renderException($output, $e);
            return Action::ACTION_NOT_FOUND;
        }
    }



}
