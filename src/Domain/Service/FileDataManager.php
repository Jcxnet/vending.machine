<?php

declare(strict_types=1);

namespace App\Domain\Service;

use App\Domain\Exception\File\FileNotFound;
use App\Domain\Exception\File\FileSaveFails;
use Symfony\Component\Filesystem\Filesystem;
use Exception;

class FileDataManager
{
    private string $dataPath;
    private Filesystem $filesystem;

    public function __construct(
        string $dataDirectory,
        Filesystem $filesystem
    ){
        $this->filesystem = $filesystem;
        $this->dataPath = $dataDirectory;
    }

    public function loadData(string $fileName): array
    {
        $fileName = $this->dataPath.$fileName;
        if (!file_exists($fileName)) {
            throw new FileNotFound();
        }
        $data = file_get_contents($fileName);

        return json_decode($data, true);
    }

    public function saveData(string $fileName, array $data): void
    {
        $fileName = $this->dataPath.$fileName;
        $data = $this->prepareData($data);

        try {
            $this->filesystem->dumpFile($fileName, $this->dataToString($data));
        } catch (Exception $exception) {
            throw new FileSaveFails();
        }
    }

    public function saveItem(string $fileName, object $object): void
    {
        $fileName = $this->dataPath.$fileName;
        try {
            $this->filesystem->dumpFile($fileName, $this->dataToString($object));
        } catch (Exception $exception) {
            throw new FileSaveFails();
        }
    }

    private function prepareData(array $data): array
    {
        $list = [];
        foreach ($data as $item) {
            if (is_object($item)) {
                $item = (array) $item;
            }
            $list[] = $item;
        }

        return $list;
    }

    private function dataToString(mixed $data): string
    {
        $pattern = "/<>[\s\S]+?<>/";
        $item = '\\u0000';
        $string = json_encode((array)$data,JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);

        return preg_replace($pattern,'',str_replace($item, "<>", $string));
    }
}
