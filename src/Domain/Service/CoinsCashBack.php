<?php

declare(strict_types=1);

namespace App\Domain\Service;

use App\Domain\Entity\Coin;

class CoinsCashBack
{
    public function __invoke(
        array $coins,
        array $credit,
        float $cashBack
    ): array {
        return  $this->getReturnCoins($coins, $credit, $cashBack);
    }

    private function getReturnCoins(
        array $coins,
        array $credit,
        float $cashBack
    ): array {

        $allCoins = $this->sortCoins($this->mergeCoins($coins, $credit));
        $returnCoins = [];
        for ($i = 0; $i < count($allCoins); $i++) {
            $coin = $allCoins[$i];
            while ((number_format($cashBack,2) >= number_format($coin->getValue(),2)) && (0 < $coin->getAmount())) {
                $cashBack -= (float)$coin->getValue();
                if (!isset($returnCoins["{$coin->getValue()}"])) {
                    $returnCoins["{$coin->getValue()}"] = 0;
                }
                $coin->setAmount($coin->getAmount()-1);
                $returnCoins["{$coin->getValue()}"] ++;
            }
        }

        if (0.0 < $cashBack) {
            return [];
        }

        $coins = [];
        foreach ($returnCoins as $value => $total){
            $coin = new Coin(
                (float) $value,
                (int) $total
            );
            $coins[] = $coin;
        }

        return $coins;
    }

    private function mergeCoins(array $coins, array $credit): array
    {
        /** @var Coin $creditCoin */
        foreach ($credit as $creditCoin) {
            /** @var Coin $coin */
            foreach ($coins as $coin) {
                if ($coin->getValue() === $creditCoin->getValue()) {
                    $coin->setAmount($coin->getAmount() + $creditCoin->getAmount());
                }
            }
        }

        return $coins;
    }

    private function sortCoins(array $coins): array
    {
        usort($coins, function ($first, $second){
           return $first->getvalue() < $second->getValue();
        });

        return $coins;
    }

}
