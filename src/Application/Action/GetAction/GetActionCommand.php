<?php

declare(strict_types=1);

namespace App\Application\Action\GetAction;

use App\Domain\Command;

class GetActionCommand implements Command
{
    private array $arguments;

    public function __construct()
    {
        $this->arguments = [];
    }

    public function setArguments(array $arguments): self
    {
        $this->arguments = $arguments;

        return $this;
    }

    public function getArguments(): array
    {
        return $this->arguments;
    }
}
