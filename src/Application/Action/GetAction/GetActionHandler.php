<?php

declare(strict_types=1);

namespace App\Application\Action\GetAction;

use App\Domain\Entity\Action;
use App\Domain\CommandHandler;

class GetActionHandler implements CommandHandler
{
    private array $listActions = [
        'RETURN-COIN',
        'GET-WATER',
        'GET-JUICE',
        'GET-SODA',
        'SERVICE',
        'SET-WATER',
        'SET-JUICE',
        'SET-SODA',
        'EXIT',
    ];

    public function __construct(private GetActionCommand $command)
    {}

    public function getAction(): Action
    {
        $arguments = $this->command->getArguments();
        $lastItem = strtoupper(end($arguments));
        $actionCode = Action::ACTION_NOT_FOUND;

        if (in_array($lastItem,$this->listActions)) {
            $actionCode = $this->getArgumentAction($lastItem);
        }
        if (Action::ACTION_NOT_FOUND === $actionCode) {
            $actionCode = $this->coinArguments($arguments);
        }
        if (Action::ACTION_NOT_FOUND !== $actionCode) {
            if (Action::ACTION_SERVICE === $actionCode || Action::ACTION_EXIT === $actionCode) {
                if (1 < count($arguments)) {
                    $actionCode = Action::ACTION_NOT_FOUND;
                }
            } else {

                if (Action::ACTION_INSERT_COINS == $actionCode) {
                    $lastItem = "INSERT-COINS";
                } else {
                    array_pop($arguments);
                    $actionCode = (Action::ACTION_INSERT_COINS !== $this->coinArguments($arguments)) ? Action::ACTION_NOT_FOUND : $actionCode;
                }
            }
        }

        return $this->createAction(
            $actionCode,
            $lastItem,
            $arguments ?? []
        );
    }

    private function createAction(
        int $actionCode,
        string $command,
        array $arguments
    ): Action {
        $action = new Action(
            strtoupper($command),
            $this->getActionDescription($actionCode),
            $actionCode
        );
        $action->setArguments($arguments);
        $action->setService($this->getActionService($actionCode));

        return $action;
    }

    private function getActionService(int $action): ?string
    {
        return match ($action){
            Action::ACTION_INSERT_COINS => 'machine.insert.coins',
            Action::ACTION_RETURN_COINS => 'machine.return.coins',
            Action::ACTION_SELL_PRODUCT => 'machine.sell.product',
            Action::ACTION_SERVICE => 'machine.service.mode',
            Action::ACTION_SET_PRODUCT => 'machine.set.product',
            default => null,
        };
    }

    private function getActionDescription(int $action): string
    {
        return match ($action){
            Action::ACTION_NOT_FOUND => 'Action not found',
            Action::ACTION_INSERT_COINS => 'Insert coins',
            Action::ACTION_RETURN_COINS => 'Return coins',
            Action::ACTION_SELL_PRODUCT => 'Sell product',
            Action::ACTION_SET_PRODUCT => 'Set product',
            Action::ACTION_SERVICE => 'Service mode',
            Action::ACTION_EXIT => 'Exit',
        };
    }

    private function coinArguments(array $arguments): int
    {
        foreach ($arguments as $argument) {
            if (!is_numeric($argument)) {
                return Action::ACTION_NOT_FOUND;
            }
        }

        return Action::ACTION_INSERT_COINS;
    }

    private function getArgumentAction(string $item): int
    {
        return match ($item){
            'RETURN-COIN' => Action::ACTION_RETURN_COINS,
            'SERVICE' => Action::ACTION_SERVICE,
            'EXIT' => Action::ACTION_EXIT,
            'GET-WATER', 'GET-JUICE', 'GET-SODA' => Action::ACTION_SELL_PRODUCT,
            'SET-WATER', 'SET-JUICE', 'SET-SODA' => Action::ACTION_SET_PRODUCT,
            default => Action::ACTION_NOT_FOUND
        };
    }
}
