<?php

declare(strict_types=1);

namespace App\Application\Machine\ServiceMode;

use App\Domain\Entity\Action;
use App\Domain\Exception\Service\ServiceNotFound;
use App\Domain\Traits\ConsoleOutputTrait;
use App\Infrastructure\Response\ResponseOk;
use App\Infrastructure\Response\ResponseWarning;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ServiceModeHandler
{
    use ConsoleOutputTrait;

    public function __invoke(
        Action $action,
        InputInterface $input,
        OutputInterface $output
    ): void {

        if (!$machineInfo = $action->getContainer()->get('machine.info')) {
            $this->renderException($output, new ServiceNotFound());
        }

        $machineInfo->setServiceMode(!$machineInfo->isServiceMode());

        if ($machineInfo->isServiceMode()) {
            $machine = $machineInfo->getMachine();
            (new ResponseWarning($input, $output))->send("Vending Machine open (service mode ON)");
            $this->renderMachineInfo($output, $machine);
        } else {
            (new ResponseOk($input, $output))->send("Vending Machine closed (service mode OFF)");
        }
    }

}
