<?php

declare(strict_types=1);

namespace App\Application\Product\SellProducts;

use App\Domain\Entity\Action;
use App\Domain\Entity\Product;
use App\Domain\Exception\Credit\CreditNotEnough;
use App\Domain\Exception\Machine\CashBackNotEnough;
use App\Domain\Exception\Product\ProductNotAvailable;
use App\Domain\Exception\Product\ProductNotFound;
use App\Domain\Exception\Service\ServiceNotFound;
use App\Domain\Traits\ConsoleOutputTrait;
use App\Infrastructure\Repository\DataCoinsRepository;
use App\Infrastructure\Repository\DataCreditRepository;
use App\Infrastructure\Repository\DataProductsRepository;
use App\Infrastructure\Response\ResponseOk;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SellProductsHandler
{
    use ConsoleOutputTrait;

    public function __invoke(
        Action $action,
        InputInterface $input,
        OutputInterface $output
    ): void {

        if (!$dataManager = $action->getContainer()->get('machine.datamanager')) {
            $this->renderException($output, new ServiceNotFound());
        }

        $creditRepository = new DataCreditRepository($dataManager);
        $coinsRepository = new DataCoinsRepository($dataManager);
        $productsRepository = new DataProductsRepository($dataManager);
        $coins = $creditRepository->getAll();

        if ($product = $this->sellProduct($action, $coins, $productsRepository, $coinsRepository, $input, $output)) {
            $productsRepository->save($product);
            $creditRepository->resetCoins();
        }
    }

    private function sellProduct(
        Action $action,
        array $creditCoins,
        DataProductsRepository $productsRepository,
        DataCoinsRepository $coinsRepository,
        InputInterface $input,
        OutputInterface $output
    ): ?Product {

        $productName = str_replace('GET-','',$action->getAction());
        $credit = $this->totalCoinsAmount($creditCoins);

        if (!$product = $productsRepository->get($productName)) {
            $this->renderException($output, new ProductNotFound());
        }
        if (0 >= $product->getAmount()) {
            $this->renderException($output, new ProductNotAvailable()) ;
        }
        if ($credit < $product->getPrice()) {
            $this->renderException($output, new CreditNotEnough());
        }
        if ($credit == $product->getPrice()) {
             $coinsRepository->saveAll($creditCoins);
        }
        $returnCoins = [];
        if ($credit > $product->getPrice()) {
            $coinsCashBack = $action->getContainer()->get('machine.cashback');
            $cashBack = $credit - $product->getPrice();
            $machineCoins = $coinsRepository->getAll();
            $returnCoins = $coinsCashBack($machineCoins, $creditCoins, $cashBack);
            if (0 == count($returnCoins)) {
                $this->renderException($output, new CashBackNotEnough());
            }
            $coinsRepository->saveAll($creditCoins);
            $coinsRepository->returnCoins($returnCoins);
        }

        $product->setAmount($product->getAmount()-1);
        $this->showProduct($product, $returnCoins, $input, $output);

        return $product;
    }

    private function showProduct(Product $product, array $coins, InputInterface $input, OutputInterface $output): void
    {
        $list = [];
        foreach ($coins as $coin) {
            for ($i = 0; $i < $coin->getAmount(); $i++){
                $list[] = sprintf("%.2f", $coin->getValue());
            }
        }
        $allCoins = $list ? ", ".(implode(', ', $list)) : "";
        $display = sprintf("-> %s%s", strtoupper($product->getName()), $allCoins);

        (new ResponseOk($input, $output))->send($display);
    }

    private function totalCoinsAmount(array $coins): float
    {
        $total = 0.0;
        foreach ($coins as $coin) {
            $total += $coin->getValue() * $coin->getAmount();
        }

        return $total;
    }

}
