<?php

declare(strict_types=1);

namespace App\Application\Product\SetProducts;

use App\Domain\Entity\Action;
use App\Domain\Entity\Product;
use App\Domain\Exception\Machine\ServiceModeInactive;
use App\Domain\Exception\Product\ProductNotFound;
use App\Domain\Exception\Service\ServiceNotFound;
use App\Domain\Traits\ConsoleOutputTrait;
use App\Infrastructure\Repository\DataProductsRepository;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SetProductsHandler
{
    use ConsoleOutputTrait;

    public function __invoke(
        Action $action,
        InputInterface $input,
        OutputInterface $output
    ): void {

        if (!$dataManager = $action->getContainer()->get('machine.datamanager')) {
            $this->renderException($output, new ServiceNotFound());
        }
        if (!$machineInfo = $action->getContainer()->get('machine.info')) {
            $this->renderException($output, new ServiceNotFound());
        }
        if (!$machineInfo->isServiceMode()) {
            $this->renderException($output, new ServiceModeInactive());
        }
        $productsRepository = new DataProductsRepository($dataManager);

        if ($product = $this->setProduct($action, $productsRepository, $output)) {
            $productsRepository->save($product);
            $this->renderProducts('Products', $productsRepository->getAll(), $output);
        }
    }

    private function setProduct(
        Action $action,
        DataProductsRepository $productsRepository,
        OutputInterface $output
    ): ?Product {

        $productName = str_replace('SET-','',$action->getAction());

        if (!$product = $productsRepository->get($productName)) {
            $this->renderException($output, new ProductNotFound());
        }
        $total = $action->getArguments();
        $product->setAmount($product->getAmount()+(int)$total[0]);

        return $product;
    }

}
