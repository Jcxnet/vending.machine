<?php

declare(strict_types=1);

namespace App\Application\Coin\InsertCoins;

use App\Domain\Entity\Action;
use App\Domain\Entity\Coin;
use App\Domain\Exception\Service\ServiceNotFound;
use App\Domain\Traits\ConsoleOutputTrait;
use App\Infrastructure\Repository\DataCoinsRepository;
use App\Infrastructure\Repository\DataCreditRepository;
use App\Infrastructure\Repository\DataRepository;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class InsertCoinsHandler
{
    use ConsoleOutputTrait;

    public function __invoke(
        Action $action,
        InputInterface $input,
        OutputInterface $output
    ): void {

        if (!$dataManager = $action->getContainer()->get('machine.datamanager')) {
           $this->renderException($output, new ServiceNotFound());
        }
        if (!$machineInfo = $action->getContainer()->get('machine.info')) {
            $this->renderException($output, new ServiceNotFound());
        }
        if ($machineInfo->isServiceMode()) {
            $coinsRepository = new DataCoinsRepository($dataManager);
            $this->insertCoins($action, $coinsRepository);
            $this->showTotalCoins('Coins', $coinsRepository, $output);

        } else {
            $creditRepository = new DataCreditRepository($dataManager);
            $this->insertCoins($action, $creditRepository);
            $this->showTotalCoins('Credit', $creditRepository, $output);
        }


    }

    private function insertCoins(Action $action, DataRepository $repository): void
    {
        $coins = [];
        foreach ($action->getArguments() as $value) {
            $coin = new Coin(
                (float) $value,
                1
            );
            $coins[] = $coin;
        }

        $repository->saveAll($coins);
    }

    private function showTotalCoins(string $title, DataRepository $repository, OutputInterface $output): void
    {
        $coins = $repository->getAll();
        $this->renderCoins($title, $coins, $output);
    }

}
