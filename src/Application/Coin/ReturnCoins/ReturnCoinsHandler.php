<?php

declare(strict_types=1);

namespace App\Application\Coin\ReturnCoins;

use App\Domain\Entity\Action;
use App\Domain\Entity\Coin;
use App\Domain\Exception\Service\ServiceNotFound;
use App\Domain\Traits\ConsoleOutputTrait;
use App\Infrastructure\Repository\DataCoinsRepository;
use App\Infrastructure\Repository\DataCreditRepository;
use App\Infrastructure\Repository\DataRepository;
use App\Infrastructure\Response\ResponseInfo;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ReturnCoinsHandler
{
    use ConsoleOutputTrait;

    public function __invoke(
        Action $action,
        InputInterface $input,
        OutputInterface $output
    ): void {

        if (!$dataManager = $action->getContainer()->get('machine.datamanager')) {
            $this->renderException($output, new ServiceNotFound());
        }
        if (!$machineInfo = $action->getContainer()->get('machine.info')) {
            $this->renderException($output, new ServiceNotFound());
        }
        $coinsRepository = new DataCoinsRepository($dataManager);
        $creditRepository = new DataCreditRepository($dataManager);

        if ($machineInfo->isServiceMode()) {
            $coins = $this->returnCoins($action, $coinsRepository);
            $this->renderCoins('Machine', $coins, $output);
            $action->setArguments([]);
        }

        $coins = $this->returnCoins($action, $creditRepository);
        $this->renderCoins('Credit', $coins, $output);

    }

    private function returnCoins(Action $action, DataRepository $repository): array
    {
        $coins = $repository->getAll();
        foreach ($action->getArguments() as $value) {
            $coin = new Coin(
                (float) $value,
                1
            );
            $coins[] = $coin;
        }

        $repository->resetCoins();

        return $coins;
    }

    private function showReturnCoins(array $coins, InputInterface $input, OutputInterface $output): void
    {
        $list = [];
        foreach ($coins as $coin) {
            for ($i = 0; $i < $coin->getAmount(); $i++){
                $list[] = sprintf("%.2f", $coin->getValue());
            }
        }
        $allCoins = $list ? implode(', ', $list) : "0.00";

        (new ResponseInfo($input, $output))->send("-> $allCoins");
    }

}
