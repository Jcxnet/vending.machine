<?php

declare(strict_types=1);

namespace App\Infrastructure\Repository;

use App\Domain\Entity\Coin;
use App\Domain\Repository\CreditRepository;
use App\Domain\Service\FileDataManager;

class DataCreditRepository extends DataRepository implements CreditRepository
{

    const FILE_DATA = "credit.json";

    public function __construct(private FileDataManager $dataManager)
    {}

    public function getAll(): array
    {
        $items = $this->dataManager->loadData(self::FILE_DATA);
        $coins = [];
        foreach ($items as $item) {
            $coin = new Coin(
                $item['value'],
                $item['amount']
            );
            $coins[] = $coin;
        }

        return $coins;
    }


    public function saveAll(array $coins): void
    {
        $allCoins = $this->getAll();

        for ($i = 0; $i < count($allCoins); $i++){
            foreach ($coins as $coin){
                if (($allCoins[$i])->getValue() == $coin->getValue()) {
                    ($allCoins[$i])->setAmount(($allCoins[$i])->getAmount() + $coin->getAmount());
                }
            }
        }

        $this->dataManager->saveData(self::FILE_DATA, $allCoins);
    }

    public function resetCoins(): void
    {
        $coins = $this->getAll();
        foreach ($coins as $coin) {
            $coin->setAmount(0);
        }

        $this->dataManager->saveData(self::FILE_DATA, $coins);
    }
}
