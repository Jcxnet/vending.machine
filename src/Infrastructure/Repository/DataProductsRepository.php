<?php

declare(strict_types=1);

namespace App\Infrastructure\Repository;

use App\Domain\Entity\Product;
use App\Domain\Repository\ProductsRepository;
use App\Domain\Service\FileDataManager;

class DataProductsRepository implements ProductsRepository
{

    const FILE_DATA = "products.json";

    public function __construct(private FileDataManager $dataManager)
    {}

    public function get(string $name): ?Product
    {
        $items = $this->dataManager->loadData(self::FILE_DATA);

        foreach ($items as $item) {
            if ($name === $item['name']) {

                return new Product(
                    $item['name'],
                    $item['price'],
                    $item['amount']
                );
            }
        }

        return null;
    }

    public function getAll(): array
    {
        $items = $this->dataManager->loadData(self::FILE_DATA);
        $products = [];
        foreach ($items as $item) {
            $product = new Product(
                $item['name'],
                $item['price'],
                $item['amount']
            );
            $products[] = $product;
        }

        return $products;
    }

    public function save(Product $product): void
    {
        $products = $this->getAll();

        for ($i = 0; $i < count($products); $i++){
            if (($products[$i])->getName() == $product->getName()) {
                $products[$i] = $product;
            }
        }

        $this->dataManager->saveData(self::FILE_DATA, $products);
    }

}
