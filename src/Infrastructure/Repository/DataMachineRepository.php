<?php

declare(strict_types=1);

namespace App\Infrastructure\Repository;

use App\Domain\Entity\Machine;
use App\Domain\Repository\MachineRepository;
use App\Domain\Service\FileDataManager;

class DataMachineRepository implements MachineRepository
{

    const FILE_DATA = "machine.json";

    public function __construct(private FileDataManager $dataManager)
    {}

    public function get(): Machine
    {
        $machineData = $this->dataManager->loadData(self::FILE_DATA);
        $machine = new Machine();
        $machine->setVersion($machineData['version']);
        $machine->setServiceMode($machineData['serviceMode']);

        return  $machine;
    }

    public function save(Machine $machine): void
    {
        $this->dataManager->saveItem(self::FILE_DATA, $machine);
    }

}
