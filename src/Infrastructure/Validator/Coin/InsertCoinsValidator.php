<?php

declare(strict_types=1);

namespace App\Infrastructure\Validator\Coin;

use App\Domain\Validator\Validator;

class InsertCoinsValidator implements Validator
{
    private array $acceptedCoins = [
        '0.05',
        '0.10',
        '0.25',
        '1',
    ];

    public function validate(array $data): bool
    {
        if (!array_key_exists('coins', $data)) {
            return false;
        }
        if (!is_array($data['coins'])) {
            return false;
        }
        foreach ($data['coins'] as $index => $coin ) {
            if (!in_array($coin, $this->acceptedCoins)) {
                return false;
            }
        }

        return true;
    }

}
