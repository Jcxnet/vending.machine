<?php

declare(strict_types=1);

namespace App\Infrastructure\Validator\Product;

use App\Domain\Validator\Validator;

class SetProductValidator implements Validator
{

    public function validate(array $data): bool
    {
        if (1 !== count($data)) {
            return false;
        }
        $value = $data[0] + 0;
        if (!is_numeric($value)) {
            return false;
        }
        if (is_float($value)) {
            return false;
        }
        if ( $value > 0 && $value < 100) {
            return true;
        }

        return false;
    }

}
