<?php

declare(strict_types=1);

namespace App\Infrastructure\Response;

class ResponseOk extends ResponseConsole
{

    public function send(mixed $data): void
    {
        $this->response($data, self::RESPONSE_SUCCESS);
    }
}
