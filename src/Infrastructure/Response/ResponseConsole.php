<?php

declare(strict_types=1);


namespace App\Infrastructure\Response;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

abstract class ResponseConsole
{
    const RESPONSE_SUCCESS = 0;
    const RESPONSE_INFO = 1;
    const RESPONSE_WARNING = 2;
    const RESPONSE_ERROR = 3;

    private SymfonyStyle $style;

    public function __construct(InputInterface $input, OutputInterface $output)
    {
        $this->style = new SymfonyStyle($input, $output);

        return $this;
    }
    public function response(mixed $data, int $responseType = self::RESPONSE_SUCCESS): void
    {
        $data = $this->formatOutput($data);

        match($responseType){
            self::RESPONSE_SUCCESS => $this->responseSuccess($data),
            self::RESPONSE_INFO => $this->responseInfo($data),
            self::RESPONSE_WARNING => $this->responseWarning($data),
            self::RESPONSE_ERROR => $this->responseError($data),
        };
    }

    private function formatOutput(mixed $data): string
    {
        if (is_string($data)) {
            return $data;
        }
        if (is_array($data) || is_object($data)) {
            return $this->dataToString($data);
        }

        return '';
    }

    private function dataToString(mixed $data): string
    {
        $pattern = "/<>[\s\S]+?<>/";
        $item = '\\u0000';
        $string = json_encode((array)$data,JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);

        return preg_replace($pattern,'',str_replace($item, "<>", $string));
    }

    private function responseSuccess(mixed $data): void
    {
        $this->style->success($data);

        //return Command::FAILURE;
    }

    private function responseInfo(mixed $data): void
    {
        $this->style->info($data);

        //return Command::FAILURE;
    }

    private function responseWarning(mixed $data): void
    {
        $this->style->warning($data);

        //return Command::SUCCESS;
    }

    private function responseError(mixed $data): void
    {
        $this->style->error($data);

        //return Command::SUCCESS;
    }
}
