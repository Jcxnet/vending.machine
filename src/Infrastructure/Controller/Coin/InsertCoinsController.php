<?php

declare(strict_types=1);

namespace App\Infrastructure\Controller\Coin;

use App\Domain\ActionService;
use App\Domain\Entity\Action;
use App\Application\Coin\InsertCoins\InsertCoinsHandler;
use App\Domain\Exception\Arguments\InvalidArguments;
use App\Domain\Traits\ConsoleOutputTrait;
use App\Infrastructure\Validator\Coin\InsertCoinsValidator;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class InsertCoinsController implements ActionService
{
    use ConsoleOutputTrait;

    public function __invoke(Action $action, InputInterface $input, OutputInterface $output): void
    {
        $validator = new InsertCoinsValidator();
        if (!$validator->validate($this->formatArguments($action->getArguments()))) {
            $this->renderException($output, new InvalidArguments());
        }

        $handler = new InsertCoinsHandler();
        $handler($action, $input, $output);
    }

    private function formatArguments(array $values): array
    {
        return [
            'coins' => $values
        ];
    }

}
