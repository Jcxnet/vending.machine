<?php

declare(strict_types=1);

namespace App\Infrastructure\Controller\Product;

use App\Domain\ActionService;
use App\Domain\Entity\Action;
use App\Domain\Exception\Arguments\InvalidArguments;
use App\Application\Coin\InsertCoins\InsertCoinsHandler;
use App\Application\Product\SellProducts\SellProductsHandler;
use App\Domain\Traits\ConsoleOutputTrait;
use App\Infrastructure\Validator\Coin\InsertCoinsValidator;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class SellProductsController implements ActionService
{
    use ConsoleOutputTrait;

    public function __invoke(Action $action, InputInterface $input, OutputInterface $output): void
    {
        if (0 < count($action->getArguments())) {
            $validator = new InsertCoinsValidator();
            if (!$validator->validate($this->formatArguments($action->getArguments()))) {
                $this->renderException($output, new InvalidArguments());
            }
            $creditHandler = new InsertCoinsHandler();
            $creditHandler($action, $input, $output);
        }

        $handler = new SellProductsHandler();
        $handler($action, $input, $output);
    }

    private function formatArguments(array $values): array
    {
        return [
            'coins' => $values
        ];
    }
}
