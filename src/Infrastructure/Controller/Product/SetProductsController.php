<?php

declare(strict_types=1);

namespace App\Infrastructure\Controller\Product;

use App\Application\Product\SetProducts\SetProductsHandler;
use App\Domain\ActionService;
use App\Domain\Entity\Action;
use App\Domain\Exception\Arguments\InvalidArguments;
use App\Domain\Traits\ConsoleOutputTrait;
use App\Infrastructure\Validator\Product\SetProductValidator;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class SetProductsController implements ActionService
{
    use ConsoleOutputTrait;

    public function __invoke(Action $action, InputInterface $input, OutputInterface $output): void
    {
        $validator = new SetProductValidator();

        if (!$validator->validate($action->getArguments())) {
            $this->renderException($output, new InvalidArguments());
        }

        $handler = new SetProductsHandler();
        $handler($action, $input, $output);
    }

}
