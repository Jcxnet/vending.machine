<?php

declare(strict_types=1);

namespace App\Infrastructure\Controller\Action;

use App\Application\Action\GetAction\GetActionCommand;
use App\Application\Action\GetAction\GetActionHandler;
use App\Domain\Entity;

final class GetActionController
{
    public function __invoke(array $arguments): Entity
    {
        $command = new GetActionCommand();
        $command->setArguments($arguments);
        $handler = new GetActionHandler($command);

        return $handler->getAction();
    }

}
