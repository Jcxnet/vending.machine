<?php

declare(strict_types=1);

namespace App\Infrastructure\Controller\Machine;

use App\Domain\ActionService;
use App\Domain\Entity\Action;
use App\Application\Machine\ServiceMode\ServiceModeHandler;
use App\Domain\Exception\Arguments\InvalidArguments;
use App\Domain\Traits\ConsoleOutputTrait;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class ServiceModeController implements ActionService
{
    use ConsoleOutputTrait;

    public function __invoke(Action $action, InputInterface $input, OutputInterface $output): void
    {
        if (0 > count($action->getArguments())) {
            $this->renderException($output, new InvalidArguments());
        }

        $handler = new ServiceModeHandler();
        $handler($action, $input, $output);
    }
}
